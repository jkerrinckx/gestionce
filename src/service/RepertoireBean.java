package service;

import java.io.File;

import application.Main;
import dao.ContactDAO;
import model.Personne;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;



public class RepertoireBean {
	private ObservableList<Personne> allContacts;
	private FilteredList<Personne> listeFiltree;
	private SortedList<Personne> personSortedList;
	private File file;
	private ContactDAO contactDAO;
	private Personne personneSelected;
	private boolean saved;
	private Personne personneSearched;
	private Main main;


	public RepertoireBean(File file , Main main) {
		personneSearched = new Personne("", "", "");
		this.file = file;
		contactDAO = new ContactDAO(file);
		allContacts = FXCollections.observableArrayList(contactDAO.lire());
		listeFiltree = new FilteredList<>(allContacts, null);
		personSortedList = new SortedList<>(listeFiltree);
		saved = true;
		this.main= main;
	}

	public void setNomSearched(String nomSearched) {
		personneSearched.setNom(nomSearched.toUpperCase());
		filtrerContact();
	}

	public void setPrenomSearched(String prenomSearched) {
		personneSearched.setPrenom(prenomSearched);
		filtrerContact();
	}

	public Personne getPersonneSelected() {
		return personneSelected;
	}

	public void setPersonneSelected(Personne personneSelected) {
		this.personneSelected = personneSelected;
	}

	public SortedList<Personne> getContacts() {
		return personSortedList;
	}

	public boolean isSaved() {
		return saved;
	}

	public void setSaved(boolean saved) {
		this.saved = this.saved && saved;
		main.setSavedState(this.saved);
	}

	public File getFile() {
		return file;
	}

	public void setFile(File newFile) {
		this.file = newFile;
		contactDAO = new ContactDAO(file);
		sauver();
	}
	
	public void ajouter() {
		if (allContacts.indexOf(personneSelected) == -1) {
			allContacts.add(personneSelected);
			saved = false;
			main.setSavedState(false);
		}
		
	}

	public void sauver() {
		contactDAO.sauver(allContacts);
		saved = true;
		main.setSavedState(true);
	}

	public void supprimer() {
		allContacts.remove(personneSelected);
		saved = false;
		main.setSavedState(false);
	}

	public void filtrerContact() {
		listeFiltree.setPredicate(personne ->
		{
			boolean c1 = personne.getNom().contains(personneSearched.getNom());
			boolean c2 = personne.getPrenom().toLowerCase().contains(personneSearched.getPrenom().toLowerCase());
			return c1 && c2;
		});
	}
}
