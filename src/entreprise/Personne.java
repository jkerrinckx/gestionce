package entreprise;

import java.time.LocalDate;


import javafx.beans.property.StringProperty;
import outils.Chaine;
import outils.OutilsDate;

public class Personne implements Comparable<Personne>
{

	private StringProperty nom;
	private StringProperty prenom;
	private StringProperty dateNaissance;

	public Personne(String nom, String prenom, String dateNaissance)
	{
		this.nom = Chaine.epureAccents(nom).toUpperCase();
		this.prenom = Chaine.setMaj(prenom);
		this.dateNaissance = OutilsDate.stringToDate(dateNaissance);
	}

	public int calculerAge()
	{
		return LocalDate.now().getYear() - dateNaissance.getYear();
	}

	public void afficherPersonne()
	{
		System.out.printf("%s %s n� le %s , age : %d ans.%n%n", nom, prenom, OutilsDate.dateToLitteral(dateNaissance),
				calculerAge());
	}

	protected String getNom()
	{
		return nom;
	}

	protected String getPrenom()
	{
		return prenom;
	}

	protected LocalDate getDateNaissance()
	{
		return dateNaissance;
	}

	@Override
	public int compareTo(Personne other)
	{
		if (dateNaissance.compareTo(other.dateNaissance) == 0)
		{
			if (nom.compareTo(other.nom) == 0)
			{
				return prenom.compareTo(other.prenom);
			}
			return nom.compareTo(other.nom);
		}
		return dateNaissance.compareTo(other.dateNaissance);
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateNaissance == null) ? 0 : dateNaissance.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		if (dateNaissance == null)
		{
			if (other.dateNaissance != null)
				return false;
		}
		else if (!dateNaissance.equals(other.dateNaissance))
		{
			return false;
		}
		if (nom == null)
		{
			if (other.nom != null)
				return false;
		}
		else if (!nom.equals(other.nom))
		{
			return false;
		}
		if (prenom == null)
		{
			if (other.prenom != null)
				return false;
		}
		else if (!prenom.equals(other.prenom))
		{
			return false;
		}
		return true;
	}

}
