package entreprise;

public class Agence {

	private String ville;
	private boolean resto;

	public Agence(String ville) {
		super();
		this.ville = ville;
		this.resto = false;
	}

	public boolean addResto() {
		return this.resto;
	}

	public boolean deleteResto() {
		return !this.resto;
	}

	public String getVille() {
		return ville;
	}

	public void afficherAgence() {
		System.out.printf("Il travaille � %s.%n", ville);
	}

	protected boolean hasResto() {
		return resto;
	}

	
	
}
