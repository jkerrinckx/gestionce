package entreprise;

import java.time.LocalDate;

import fr.jk.outils.OutilsDate;

public class DroitCE {

	private static final int ANCIENNETE = 9;
	private static final String DATELIMITE = "01/07/";
	private static final int AGE1 = 10;
	private static final int CHEQUE1 = 20;
	private static final int AGE2 = 15;
	private static final int CHEQUE2 = 30;
	private static final int AGE3 = 18;
	private static final int CHEQUE3 = 50;
	private Salarie salarie;

	public DroitCE(Salarie salarie) {
		super();
		this.salarie = salarie;
	}

	private boolean chequesVacances() {
		LocalDate base = OutilsDate.stringToDate(DATELIMITE + LocalDate.now().getYear());
		return base.minusMonths(ANCIENNETE).compareTo(salarie.getDateEntree()) >= 0;
	}


	private int chequesCadeaux() {
		int montant = 0;
		for (int i = 0; i < salarie.getEnfants().size(); i++) {
			int age = salarie.getEnfants().get(i).calculerAge();
			if (age <= AGE1)
				montant += CHEQUE1;
			else if (age <= AGE2)
				montant += CHEQUE2;
			else if (age <= AGE3)
				montant += CHEQUE3;
		}
		return montant;
	}

	protected void afficherDroits() {
		if (chequesVacances())
			System.out.println("Le salari� a droit aux ch�ques vacances.");
		else
			System.out.println("Le salari� n'a pas droit aux ch�ques vacances.");
		System.out.println("Le salari� a droit � " + chequesCadeaux() + " � de ch�ques cadeaux.");
		System.out.println();
	}
	
}
