package entreprise;

import java.time.LocalDate;

import java.util.ArrayList;

import fr.jk.outils.OutilsDate;

public class Salarie extends Personne {

	private LocalDate dateEntree;
	private ArrayList<Personne> enfants;
	private DroitCE droits;
	private Agence agence;
	

	public Salarie(String nom, String prenom, String dateNaissance, String dateEntree ) {
		super(nom, prenom, dateNaissance);
		this.dateEntree = OutilsDate.stringToDate(dateEntree);
		enfants = new ArrayList<>();
		droits = new DroitCE(this);
	
	}

	public void changerAgence(Agence agence) {
		this.agence = agence;
	}

	public void ajouterEnfant(Personne enfant) {
		enfants.add(enfant);
		enfants.sort(null);
	}
	
	public void supprimerEnfant(Personne enfant)
	{
		enfants.remove(enfant);
	}

	protected LocalDate getDateEntree() {
		return dateEntree;
	}

	protected DroitCE getDroits() {
		return droits;
	}

	private void afficherEnfants() {
		System.out.println("Ses enfants sont : ");
		System.out.println();
		for (int i = 0; i < enfants.size(); i++) {
			getEnfants().get(i).afficherPersonne();
		}
	}

	private void afficherRestoOuTickets()
	{
		if (agence.hasResto()) System.out.println("Son agence a un restaurant d'entreprise.");
		else System.out.println("Son agence donne des tickets restaurant.");
	}
	
	ArrayList<Personne> getEnfants() {
		return enfants;
	}

	private void afficherSalarie()
	{
		System.out.printf("Voici %s %s n� le %s , il travaille dans l'agence de %s depuis le %s.%n%n" , getPrenom() , getNom() , OutilsDate.dateToLitteral(getDateNaissance()) , agence.getVille() , OutilsDate.dateToString(dateEntree ));
	}
	
	public void afficher() {
		afficherSalarie();
		afficherRestoOuTickets();
		droits.afficherDroits();
		afficherEnfants();
		}
}
