package vue;

import java.io.File;

import application.Main;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import service.RepertoireBean;

public class MenuController
{

	private Main main;
	private static final String EXTENTIONTEXT = "Text Files";
	private static final String EXTENTIONTXT = "*.txt";
	


	public void setMain(Main gestionContact)
	{
		this.main = gestionContact;
	}

	@FXML
	private void nouveau()
	{
		main.sauver();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter(EXTENTIONTEXT, EXTENTIONTXT));
		fileChooser.setInitialDirectory(new File(main.getParametres().getDernierRepertoire()));
		File selectedFile = fileChooser.showSaveDialog(main.getPrimaryStage());
		if (selectedFile != null)
		{
			majParametres(selectedFile);
			main.setRepertoire(new RepertoireBean(selectedFile, main));
			main.setFileTitle(selectedFile.getPath());
		}
	}

	@FXML
	private void openFile()
	{
		main.sauver();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Ouvrir un fichier de contacts");
		fileChooser.getExtensionFilters().add(new ExtensionFilter(EXTENTIONTEXT, EXTENTIONTXT));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Csv Files", "*.csv"));
		fileChooser.setInitialDirectory(new File(main.getParametres().getDernierRepertoire()));
		File selectedFile = fileChooser.showOpenDialog(main.getPrimaryStage());
		if (selectedFile != null)
		{
			majParametres(selectedFile);
			main.setRepertoire(new RepertoireBean(selectedFile, main));
			main.setFileTitle(selectedFile.getPath());
		}
	}

	@FXML
	private void saveFile()
	{
			if (!main.getRepertoire().isSaved()) {
			main.getRepertoire().sauver();}
	}

	@FXML
	private void fichierFermer()
	{
		main.fermer();
		main.setFileTitle(null);
	}

	@FXML
	private void saveAs()
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Enregistrer sous : ");
		fileChooser.getExtensionFilters().add(new ExtensionFilter(EXTENTIONTEXT, EXTENTIONTXT));
		fileChooser.getExtensionFilters().add(new ExtensionFilter("Csv Files", "*.csv"));
		fileChooser.setInitialDirectory(new File(main.getParametres().getDernierRepertoire()));
		File selectedFile = fileChooser.showSaveDialog(main.getPrimaryStage());
		if (selectedFile != null)
		{
			majParametres(selectedFile);
			main.getRepertoire().setFile(selectedFile);
			main.setFileTitle(selectedFile.getPath());
		}
		
		
	}

	@FXML
	private void quit()
	{
			main.sauver();
			Platform.exit();
		}

	@FXML
	private void openHelp()
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Gestion de contacts");
		alert.setHeaderText("A propos de :");
		alert.setContentText("Exemple d'application JavaFX : Julien KERRINCKX");
		alert.showAndWait();
	}

	private void majParametres(File selectedFile)
	{
		main.getParametres().setDernierRepertoire(selectedFile.getAbsolutePath());
		String fichier = selectedFile.getAbsolutePath();
		int position = main.getParametres().getFichiersRecents().indexOf(fichier);
		if (position != -1)
		{
			main.getParametres().getFichiersRecents().remove(position);
		}
		main.getParametres().getFichiersRecents().add(0, selectedFile.getAbsolutePath());
	}
}
